package com.irisking.scanner;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.Size;
import android.os.Bundle;
import android.os.IBinder;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.irisking.irisappa.R;

//主文件，完成界面显示，UI控件控制等逻辑
public class StartActivity extends Activity{
	
	private int mCameraId;

	private RadioButton frontRadioBtn;
	private RadioButton rearRadioBtn;
	private Button confirmBtn;

	private EditText mEtCameraID;
	private EditText mEtPreviewSize;
	private ListView listView;
	
	private ArrayList<Size> mPreviewSizeList;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_iris_start);

		frontRadioBtn = (RadioButton) findViewById(R.id.front_camera_id);
		rearRadioBtn = (RadioButton) findViewById(R.id.rear_camera_id);
		confirmBtn = (Button) findViewById(R.id.btn_ok);
		
		frontRadioBtn.setChecked(true);
		mCameraId = 3;

		// mEtCameraID = (EditText) findViewById(R.id.ais_id);
		// mEtPreviewSize = (EditText) findViewById(R.id.ais_previewsize);
		// listView = (ListView) findViewById(R.id.ais_listview);
		frontRadioBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mCameraId = 3;
			}
		});
		
		rearRadioBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mCameraId = 2;
			}
		});
		
		confirmBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
//				Log.e("tony", "选中的摄像头ID: " + mCameraId);
				Camera camera = null;
				camera = Camera.open(mCameraId);
				
				Intent intent = new Intent(StartActivity.this, MainActivity.class);
				intent.putExtra("cameraid", mCameraId);
				intent.putExtra("width", 1920);
				intent.putExtra("height", 1080);
				startActivity(intent);
				
				 if(camera != null){
					 camera.release();
				 }
			}
		});
	}

//	public void onClick(View view) {
//
//		switch (view.getId()) {
//			case R.id.front_camera_id:
//				mCameraId = 3;
//				break;
//			case R.id.rear_camera_id:
//				mCameraId = 2;
//				break;
//			case R.id.btn_ok:
//				Log.e("tony", "选中的摄像头ID: " + mCameraId);
//				Camera camera = null;
//				camera = Camera.open(mCameraId);
//				
//				Intent intent = new Intent(StartActivity2.this, MainActivity.class);
//				intent.putExtra("cameraid", mCameraId);
//				intent.putExtra("width", 1920);
//				intent.putExtra("height", 1080);
//				startActivity(intent);
//				
//				 if(camera != null){
//					 camera.release();
//				 }
//				break;
//			default:
//				break;
//		}

		// String cameraid = mEtCameraID.getText().toString();
		// if(TextUtils.isEmpty(cameraid)){
		// Toast.makeText(this , "请输入CameraID", Toast.LENGTH_SHORT).show();
		// return;
		// }
		//
		// try {
		// mCameraId = Integer.valueOf(cameraid);
		// } catch (NumberFormatException e) {
		// Toast.makeText(this , "请输入正确的CameraID", Toast.LENGTH_SHORT).show();
		// return;
		// }
		// Camera camera = null;
		// try {
		// camera = Camera.open(mCameraId);
		// } catch (Exception e) {
		// Toast.makeText(this , "无法找到CameraID " + mCameraId + " 对应的设备，请重新输入",
		// Toast.LENGTH_SHORT).show();
		// listView.setEnabled(false);
		// return;
		// } finally{
		// }
		//
		// mPreviewSizeList = getSupportPreviewSize(camera/*, mCameraId*/);
		// listView.setEnabled(true);
		// listView.setVisibility(View.VISIBLE);
		// listView.setAdapter(new MyBaseAdapter());
		// listView.setOnItemClickListener(new MyOnItemClickListener());
		//
		// Toast.makeText(this , "请选择preview size", Toast.LENGTH_SHORT).show();
		//
		// if(camera != null){
		// camera.release();
		// }
//	}

	private ArrayList<Size> getSupportPreviewSize(Camera mCamera/*
																 * , int
																 * cameraId
																 */) {

		ArrayList<Size> list = new ArrayList<Size>();

    	if(mCamera == null){
    		return list;
    	}
    	
//    	Camera mCamera = Camera.open(cameraId);
    	Parameters parameters = mCamera.getParameters();
    	
    	List<Size> supportedPreviewSizes = parameters.getSupportedPreviewSizes();
    	
    	for(Size size : supportedPreviewSizes){
    		if(size.width == 1920){
    			list.add(size);
    		}
    	}
//    	mCamera.release();
    	
    	return list;
    }
    
	private class MyBaseAdapter extends BaseAdapter{

		@Override
		public int getCount() {

			return mPreviewSizeList.size();
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {

			return position;
		}

		@SuppressLint("ViewHolder")
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			View view = View.inflate(StartActivity.this, R.layout.view, null);
			
			TextView tv = (TextView) view.findViewById(R.id.view);
			tv.setText(mPreviewSizeList.get(position).width + " * " + mPreviewSizeList.get(position).height);
			
			return view;
		}
	}
	
	private class MyOnItemClickListener implements AdapterView.OnItemClickListener{
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id){
	    	
			mEtPreviewSize.setText(mPreviewSizeList.get(position).width + " * " + mPreviewSizeList.get(position).height);
			
	    	Intent intent = new Intent(StartActivity.this, MainActivity.class);
	    	intent.putExtra("cameraid", mCameraId);
	    	intent.putExtra("width", mPreviewSizeList.get(position).width);
	    	intent.putExtra("height", mPreviewSizeList.get(position).height);
	    	startActivity(intent);
		}
	}
	/**
	 * 当EditText输入完毕后，点击空白区域隐藏软键盘
	 * */
	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		if(ev.getAction() == MotionEvent.ACTION_DOWN){
			View view = getCurrentFocus();
			if (isShouldHideKeyboard(view, ev)) {
                hideKeyboard(view.getWindowToken());
            }
		}
		return super.dispatchTouchEvent(ev);
	}
	private boolean isShouldHideKeyboard(View view , MotionEvent event){
		if(view != null && (view instanceof EditText)){
			int[] location = {0,0};
			view.getLocationInWindow(location);
			int left = location[0],
					top = location[1],
					bottom = top + view.getHeight(),
					right = left + view.getWidth();
			if (event.getX() > left && event.getX() < right
                    && event.getY() > top && event.getY() < bottom) {
                // 点击EditText的事件，忽略它。
                return false;
            } else {
                return true;
            }
		}
		// 如果焦点不是EditText则忽略，这个发生在视图刚绘制完，第一个焦点不在EditText上，和用户用轨迹球选择其他的焦点
		return false;
	}
	/**
     * 获取InputMethodManager，隐藏软键盘
     */
	private void hideKeyboard(IBinder token) {
        if (token != null) {
            InputMethodManager im = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            im.hideSoftInputFromWindow(token, InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
}
